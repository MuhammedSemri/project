package handlers

import (
	"testing"
)



func Test_login(t *testing.T) {
	const schema = `
	{
		"email":"ter@proj.com",
		"pass": "lol12345"
	}`



	test.Post("/login").
		Expect(t).
		Status(200).
		Type("json").
		JSONSchema(schema).
		Done()
}