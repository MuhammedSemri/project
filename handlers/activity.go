package handlers

import (
	"github.com/gin-gonic/gin/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"project/authentication"
	"project/models"
)

func createActivity(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	var act models.Activity

	// parse user ID from Context
	cl := authentication.ParseClaimsContext(req)
	// parse json request to struct
	ParseJson(res, req, &act)
	// copy db session
	act.DB = Cfg.DB
	// convert cl["sub"] of type interface to string

	act.UserID = cl["sub"].(string)
	ok := act.InsertActivity()
	if !ok {
		res.WriteHeader(http.StatusForbidden)
		res.Write([]byte("server error"))
		return
	}
	log.Println(cl["sub"])

	res.Write([]byte("Success"))
}

func updateActivity(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	var act models.Activity

	// parse user ID from Context
	cl := authentication.ParseClaimsContext(req)
	// parse json request to struct
	ParseJson(res, req, &act)
	act.DB = Cfg.DB
	// convert cl["sub"] of type interface to string
	act.UserID = cl["sub"].(string)

	ok, err := act.UpdateActivity()
	if !ok && err == nil {
		res.WriteHeader(http.StatusNotFound)
		res.Write([]byte("Activity not found"))
		return
	} else if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Write([]byte("success"))
	return
}

func deleteActivity(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	vars := mux.Vars(req)
	actID := vars["activityid"]
	if actID == "" {
		// no activity id was sent with request
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte("no id in request"))
		return
	}
	// get claims from context
	cl := authentication.ParseClaimsContext(req)
	// parse user id from jwt claims
	uid := cl["sub"].(string)
	ok, err := models.DeleteActivity(actID, uid, Cfg.DB)
	if !ok && err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	} else if !ok && err == nil {
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	res.WriteHeader(http.StatusOK)
	return
}

func getActivities(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	cl := authentication.ParseClaimsContext(req)
	// parse user id from jwt claims
	uid := cl["sub"].(string)
	// TODO: activities parse should be limited to 20 per request
	acts, ok, err := models.GetActivitysByUserID(Cfg.DB, uid)

	if !ok && err == nil {
		res.WriteHeader(http.StatusOK)
		res.Write([]byte("No activities found"))
		return
	} else if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		res.Write([]byte(err.Error()))
		return
	}

	resData, err := json.Marshal(acts)
	if err != nil {
		log.Println(err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Write(resData)
	return
}

