package handlers

import (
	"gopkg.in/h2non/baloo.v3"
	"testing"
)

const v1api = "/v1/api"

var test = baloo.New("http://localhost:8080")

const schemaCreate = `{
	"name": "test",
	"desc": "just tesing some stuff",
	"type": "job"
	}`

func Test_createActivity(t *testing.T) {
	test.Post("/v1/activity/create").
		SetHeader("X-Auth-Token", "bearer eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MzE3Nzk0MzcsImlhdCI6MTUzMTUyMDIzNywic3ViIjoiMjk4YjVlM2QtMGIwOS00NjgyLWE5MTAtN2UzZWU3ZWUzY2JhIn0.fZJ_n8H8gw926_4IIU2rjd0b-olqy4tVEXStZaRlppoapgsHEJEFuBH7SIVvQNEI0CpCvAuvpAy103-thKXdUKOX4bFW2a69TFaa8LnZ6j4s-Xzn3VQInEvN_SMvg0z6-_ncQfuWKOu1bK7NKTBkSLAGzieupb0q_46yuQpwjJna93IsCTnddMdjvRknOQEESyR0bfLL9-oLgk9QstsjSdRGADNea5En-gBZmjUsyRcjPiuaj2zG4NfdTwn_thmZeiyeOb5c2l4QbgEV_N7nBE79ZN48AsSliNqLtIk6MeWSwmEOYCVqgiF7XYxqFARSwOLh3Oz-6Qz5UVDHhAMj9A").
		Expect(t).
		Status(200).
		Header("Content-Type", "json").
		Type("json").
		JSONSchema(schemaCreate).
		Done()
}
