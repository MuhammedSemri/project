package handlers

import (
	"database/sql"
	"github.com/codegangsta/negroni"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gopkg.in/go-playground/validator.v9"
	"io"
	"log"
	"net/http"
	"project/authentication"
	"project/settings"
)

// Config contains the DB session and valdatior
type Config struct {
	Validate *validator.Validate
	DB       *sql.DB
}

//Cfg is a Global variable that contains the db session and validator
var Cfg *Config

// Serve start the http server and set the routes
func Serve() {
	settings.Init()
	Cfg = NewConfig()
	defer Cfg.DB.Close()
	router := mux.NewRouter()
	router.HandleFunc("/health", healthCheckHandler)
	subRouterv1 := router.PathPrefix("/v1").Subrouter()
	subRouterAct := subRouterv1.PathPrefix("/activity").Subrouter()
	subRouterv1.HandleFunc("/signup", signup).Methods("POST")
	router.HandleFunc("/login", login).Methods("POST")
	subRouterAct.Handle("/create",
		negroni.New(
			negroni.HandlerFunc(authentication.RequireTokenAuthentication),
			negroni.HandlerFunc(createActivity),
		)).Methods("POST")
	subRouterAct.Handle("/update",
		negroni.New(
			negroni.HandlerFunc(authentication.RequireTokenAuthentication),
			negroni.HandlerFunc(updateActivity),
		)).Methods("POST")
	subRouterAct.Handle("/delete/{activityid}",
		negroni.New(
			negroni.HandlerFunc(authentication.RequireTokenAuthentication),
			negroni.HandlerFunc(updateActivity),
		)).Methods("DELETE")
	subRouterAct.Handle("/getall",
		negroni.New(
			negroni.HandlerFunc(authentication.RequireTokenAuthentication),
			negroni.HandlerFunc(getActivities),
		)).Methods("GET")
	n := negroni.Classic()
	n.UseHandler(router)
	// change the cors configs
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"http://192.168.232.1"},
		AllowedMethods: []string{"POST","GET"},
	})
	n.Use(c)
	err := http.ListenAndServe(":8080", n)
	if err != nil {
		log.Fatal(err)
	}
}

// NewConfig returns new db session and validator
func NewConfig() *Config {
	return &Config{
		Validate: newValidator(),
		DB:       ConnectDB(),
	}
}

// ConnectDB connects to the database
// returns the connection session
func ConnectDB() *sql.DB {
	log.Println("running")
	db, err := sql.Open("mysql", "root:ls123456@/db")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Connected to the DB")
	return db
}

// registers new validation for the structs using validator package
// and returns *validator.Validate
func newValidator() *validator.Validate {
	val := validator.New()
	val.RegisterValidation("is-username", ValidateUsernameLen)
	val.RegisterValidation("is-pass", ValidatePassLen)
	val.RegisterValidation("is-alphau", IsAlpha)
	val.RegisterValidation("is-alphap", IsNotAlpha)
	return val
}

func healthCheckHandler(res http.ResponseWriter, req *http.Request) {
	// A very simple health check.
	res.WriteHeader(http.StatusOK)
	res.Header().Set("Content-Type", "application/json")

	// In the future we could report back on the status of our DB, or our cache
	// (e.g. Redis) by performing a simple PING, and include them in the response.
	io.WriteString(res, `{"alive": true}`)
}
