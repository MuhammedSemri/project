package handlers

import (
	"gopkg.in/go-playground/validator.v9"
	"regexp"
)

// ValidateUsernameLen returns false if username is too short x < 4
func ValidateUsernameLen(fl validator.FieldLevel) bool {
	return len(fl.Field().String()) > 3
}

// ValidatePassLen returns false if password > 7 or doesn't contain alphanumeric characters
func ValidatePassLen(fl validator.FieldLevel) bool {
	return len(fl.Field().String()) > 7

}

// IsAlpha checks if string contains alphapetic characters only
// return true if it is
// returns false if it contains numbers or sympols
func IsAlpha(fl validator.FieldLevel) bool {
	isAlpha := regexp.MustCompile(`^[A-Za-z]+$`).MatchString
	if isAlpha(fl.Field().String()) {
		return true
	}
	return false
}

// IsNotAlpha returns true if field contains sympols or numbers
func IsNotAlpha(fl validator.FieldLevel) bool {
	isAlpha := regexp.MustCompile(`^[A-Za-z]+$`).MatchString
	if isAlpha(fl.Field().String()) {
		return false
	}
	return true
}
