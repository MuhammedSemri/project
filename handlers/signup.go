package handlers

import (
	"encoding/json"
	"errors"
	"github.com/satori/go.uuid"
	"gopkg.in/go-playground/validator.v9"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"project/models"
)

// ErrRequired missing field error
var ErrRequired = errors.New("Missing field")

// ErrUsernameShort username should contain atleast 4 characters
var ErrUsernameShort = errors.New("Username should contain atleast 4 characters")

// ErrUsernameAlpha username should contain atleast 4 characters
var ErrUsernameAlpha = errors.New("Username should not contain numbers and sympols")

// ErrUsernameExists username already in use error
var ErrUsernameExists = errors.New("Username already in use")

// ErrEmailValid email is not valid error
var ErrEmailValid = errors.New("Please Enter a valid Email")

// ErrEmailExists Email already in use error
var ErrEmailExists = errors.New("Email already in use")

// ErrPassLen password is too short error
var ErrPassLen = errors.New("Password should be longer than 7 characters")

// ErrPassMatch passwords dont match error
var ErrPassMatch = errors.New("Passwords don't match")

// ErrPassAlpha password shouldn't contain Alpha charecters only error
var ErrPassAlpha = errors.New("Password should contain numbers and sympols")

// ErrCreatingUser server error while creating user
var ErrCreatingUser = errors.New("Error creating user")

// SignupForm is the form fields
type SignupForm struct {
	// Username string `validate:"required,is-username,is-alphau" json:"username"`
	Email    string `validate:"required,email" json:"email"`
	Pass     string `validate:"required,is-pass,is-alphap" json:"pass"`
	Repass   string `validate:"required" json:"repass"`
}

type response struct {
	Err     string `json:"error"`
	Created bool   `json:"created"`
}

func (sgn *SignupForm) formValid() (bool, error) {

	err := Cfg.Validate.Struct(sgn)
	if err != nil {
		return false, err
	}
	return true, nil
}

func signup(res http.ResponseWriter, req *http.Request) {
	
	var form SignupForm

	body, err := ioutil.ReadAll(io.LimitReader(req.Body, 1048576))

	if err != nil {
		panic(err)
	}
	if err := req.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &form); err != nil {
		res.Header().Set("Content-Type", "application/json; charset=UTF-8")
		res.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(res).Encode(err); err != nil {
			log.Fatal(err)
		}
	}

	ok := CheckForm(res, form)
	if !ok {
		return
	}
	ret := response{
		Created: true,
	}

	resJSON, err := json.Marshal(ret)
	if err != nil {
		log.Fatal(err)
	}
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	res.Header().Set("Access-Control-Allow-Headers",
		"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	res.Write(resJSON)
	
	return
}

// CheckForm validates the login reuest data
// return true if everythin is ok
// if false it responds to the user and returns false
// if false after returning the request stops
func CheckForm(res http.ResponseWriter, form SignupForm) bool {
	type response struct {
		Err     string `json:"error"`
		Created bool   `json:"created"`
	}

	if _, err := form.formValid(); err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			switch err.Tag() {
			case "required":
				errorResp(res, ErrRequired)
				return false
			case "is-username":
				errorResp(res, ErrUsernameShort)
				return false
			case "is-alphau":
				errorResp(res, ErrUsernameAlpha)
				return false
			case "email":
				errorResp(res, ErrEmailValid)
				return false
			case "is-pass":
				errorResp(res, ErrPassLen)
				return false
			case "is-alphap":
				errorResp(res, ErrPassAlpha)
				return false
			}
		}
		return false
	}
	// TODO: use only one struct
	searchUser := models.User{
		DB:       Cfg.DB,
		// Username: form.Username,
		Email:    form.Email,
	}
	// ok := searchUser.UsernameExists()
	// if ok {
	// 	errorResp(res, ErrUsernameExists)
	// 	return false
	// }
	ok := searchUser.EmailExists()
	if ok {
		errorResp(res, ErrEmailExists)
		return false
	}
	if form.Pass != form.Repass {
		errorResp(res, ErrPassMatch)
		return false
	}

	newUsr := models.User{
		DB:       Cfg.DB,
		// Username: form.Username,
		Email:    form.Email,
		Password: form.Pass,
	}
	newID(&newUsr)
	ok = newUsr.CreateUser()
	if !ok {
		errorResp(res, ErrCreatingUser)
		return false
	}
	return true
}

// Return an error as a response
func errorResp(res http.ResponseWriter, msg error) {
	ret := response{
		Err:     msg.Error(),
		Created: false,
	}

	resJSON, err := json.Marshal(ret)
	if err != nil {
		log.Fatal(err)
	}
	res.Write(resJSON)
}

// Generarates a new UUID for the user
// uses recursion to check if id already exists
func newID(usr *models.User) {
	id := uuid.NewV4()
	usr.ID = id.String()

	exists := usr.IDExists()
	// if id already exists in the db
	if exists {
		newID(usr)
	}

}
