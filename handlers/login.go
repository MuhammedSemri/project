package handlers

import (
	"encoding/json"
	"errors"
	"golang.org/x/crypto/bcrypt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"project/authentication"
	"project/models"
)

// ErrLoggingIn there is a problem with the server
var ErrLoggingIn = errors.New("Can't login right now")

// ErrWrongPass is returned if the password was wrong
var ErrWrongPass = errors.New("Wrong password")

// ErrWrongEmail is returned if the email is not valid or not in use
var ErrWrongEmail = errors.New("Wrong Email")

// Response is the response to te request
type Response struct {
	Error  string `json:"error"`
	Logged bool   `json:"logged"`
	Token  string `json:"token"`
}

// LoginForm Post request data
type LoginForm struct {
	Email string `json:"email"`
	Pass  string `json:"pass"`
}

func login(res http.ResponseWriter, req *http.Request) {
	var form LoginForm

	body, err := ioutil.ReadAll(io.LimitReader(req.Body, 1048576))

	if err != nil {
		panic(err)
	}
	if err := req.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &form); err != nil {
		res.Header().Set("Content-Type", "application/json; charset=UTF-8")
		res.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(res).Encode(err); err != nil {
			log.Fatal(err)
		}
		return
	}
	usr := models.User{
		DB:    Cfg.DB,
		Email: form.Email,
	}
	ok, err := usr.GetUser()
	if !ok {
		checkErr(res, err)
		resData := Response{
			Error:  ErrWrongEmail.Error(),
			Logged: false,
		}
		resJSON, err := json.Marshal(&resData)
		if err != nil {
			log.Println(err)
			res.Write([]byte(ErrLoggingIn.Error()))
			return
		}
		res.WriteHeader(http.StatusUnauthorized)
		res.Write(resJSON)
		return
	}
	if err := bcrypt.CompareHashAndPassword([]byte(usr.Password), []byte(form.Pass)); err != nil {
		respond(res, ErrWrongPass, "")
		return
	}
	auth := authentication.InitJWTAuthenticationBackend()

	log.Println(usr.ID)
	token, err := auth.GenerateToken(usr.ID)
	if err != nil {
		checkErr(res, err)
		return
	}

	respond(res, nil, token)
	return
}

// Checks if error exists
// and responds it !nil with the error data to the client
func checkErr(res http.ResponseWriter, err error) {

	if err != nil {
		resData := Response{
			Error:  err.Error(),
			Logged: false,
		}
		resJSON, err := json.Marshal(&resData)
		if err != nil {
			log.Println(err)
			res.Write([]byte(ErrLoggingIn.Error()))
			return
		}
		res.WriteHeader(http.StatusUnauthorized)
		res.Write(resJSON)
		return
	}
}

// respond returns json data if user was authenticated
func respond(res http.ResponseWriter, errInp error, token string) {
	resData := Response{
		Error:  "",
		Logged: true,
		Token:  token,
	}
	resJSON, err := json.Marshal(resData)
	if err != nil {
		log.Println(err)
		res.Write([]byte(ErrLoggingIn.Error()))
		return
	}
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(http.StatusOK)
	res.Write(resJSON)
	return
}
