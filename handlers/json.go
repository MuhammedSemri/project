package handlers

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

func ParseJson(res http.ResponseWriter, req *http.Request, reqData interface{}) {
	body, err := ioutil.ReadAll(io.LimitReader(req.Body, 1048576))

	if err != nil {
		log.Println(err)
		res.WriteHeader(http.StatusForbidden)
		return
	}
	if err := req.Body.Close(); err != nil {
		log.Println(err)
		res.WriteHeader(http.StatusForbidden)
		return
	}
	if err := json.Unmarshal(body, &reqData); err != nil {
		res.Header().Set("Content-Type", "application/json; charset=UTF-8")
		res.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(res).Encode(err); err != nil {
			res.WriteHeader(http.StatusForbidden)
			log.Println(err)
			return
		}
	}
}
