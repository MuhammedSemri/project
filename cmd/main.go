package main

import (
	"project/handlers"
)

func main() {
	handlers.Serve()
}
