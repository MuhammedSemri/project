package settings

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

var environments = map[string]string{
	"production":    "C:/Users/User/Documents/golang/src/project/settings/prod.json",
	"preproduction": "C:/Users/User/Documents/golang/src/project/settings/pre.json",
	"tests":         "../../settings/tests.json",
}

// Settings is the app settings
type Settings struct {
	PrivateKeyPath     string
	PublicKeyPath      string
	JWTExpirationDelta int
}

var settings Settings
var env = "preproduction"

// Init loades the app settings
// reads enviromental variables
func Init() {
	env = os.Getenv("GO_ENV")
	if env == "" {
		fmt.Println("Warning: Setting preproduction environment due to lack of GO_ENV value")
		env = "preproduction"
	}
	LoadSettingsByEnv(env)
}

// LoadSettingsByEnv reads the JSON setting files
func LoadSettingsByEnv(env string) {
	content, err := ioutil.ReadFile(environments[env])
	if err != nil {
		fmt.Println("Error while reading config file", err)
	}
	settings = Settings{}
	jsonErr := json.Unmarshal(content, &settings)
	if jsonErr != nil {
		fmt.Println("Error while parsing config file", jsonErr)
	}
}

// GetEnvironment return GO_ENV system variable
func GetEnvironment() string {
	return env
}

// Get returns the app a new Settings containing the app settings
func Get() Settings {
	if &settings == nil {
		Init()
	}
	return settings
}

// IsTestEnvironment returns true if app is in testing mode
func IsTestEnvironment() bool {
	return env == "tests"
}
