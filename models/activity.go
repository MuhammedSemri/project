package models

import (
	"database/sql"
	"log"
)

// Activity  is the descriptions of the event like math class wedding work
// Activity.Type is for example job class hackathon
type Activity struct {
	DB          *sql.DB
	ID          string    `json:"id"`
	UserID      string    `json:"uid"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Type        string    `json:"type"`
	Sessions    []Session `json:"sessions"`
}

type Session struct {
	DB         *sql.DB
	ID         string `json:"id"`
	ActivityID string `json:"actid"`
	Date       string `json:"date"`
	StartTime  string `json:"start"`
	EndTime    string `json:"end"`
	Location   string `json:"location"`
	Notes      []Note `json:"notes"`
}

// NewActivity builder
func NewActivity(db *sql.DB, usrID, name, desc, typ string) Activity {
	return Activity{
		DB:          db,
		ID:          GenerateID(),
		UserID:      usrID,
		Name:        name,
		Description: desc,
		Type:        typ,
	}
}

// SetUserID AYE
func (act *Activity) SetUserID(uid string) {
	act.UserID = uid
}

// InsertActivity Save a new activity in the DB
func (act *Activity) InsertActivity() bool {
	act.ID = GenerateID()
	stmtIns, err := act.DB.Prepare("INSERT Activity SET ID=?,UserID=?, ActivityName=?,Description=?,ActivityType=?")
	if err != nil {
		log.Println(err)
		return false
	}
	defer stmtIns.Close()
	_, err = stmtIns.Exec(act.ID, act.UserID, act.Name, act.Description, act.Type)
	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}

// UpdateActivity update existing activity in the DB
func (act *Activity) UpdateActivity() (bool, error) {
	stmtIns, err := act.DB.Prepare("UPDATE Activity SET ActivityName=?,Description=?,ActivityType=? WHERE ID=? AND UserID=?")
	if err != nil {
		log.Println(err)
		return false, err
	}
	defer stmtIns.Close()
	result, err := stmtIns.Exec(act.Name, act.Description, act.Type, act.ID, act.UserID)
	if err == sql.ErrNoRows {
		log.Println(err)
		return false, nil
	} else if err != nil {
		log.Println(err)
		return false, err
	}
	log.Println(result.RowsAffected())
	return true, nil
}

// DeleteActivity deletes an activity from DB
// uid is the user id
// id is the activity id
func DeleteActivity(id, uid string, db *sql.DB) (bool, error) {
	stmt, err := db.Prepare("delete from Activity where ID=? and UserID=?")
	// FIXME
	// FIXME
	// FIXME
	// FIXME
	if err == sql.ErrNoRows {
		return false, nil
	} else if err != nil {
		return false, err
	}

	_, err = stmt.Exec(id, uid)
	if err == sql.ErrNoRows {
		return false, nil
	} else if err != nil {
		return false, err
	}
	return true, nil
}

// getActivityByID take an Activity ID and returns it's sessions
func (act *Activity) GetActivityByID(id string) (bool, error) {
	// FIXME
	err := act.DB.QueryRow("SELECT * FROM Session WHERE ID=?", id).Scan(&act.UserID, &act.Name, &act.Description, &act.Type)

	if err == sql.ErrNoRows {
		return false, nil
	} else if err != nil {
		return false, err
	}

	return true, nil
}

func ActivityExist(db *sql.DB, uid string) (bool, error) {
	var act Activity
	err := db.QueryRow("select * from Activity where UserID = ?", uid).Scan(&act.ID, &act.UserID, &act.Name, &act.Description, &act.Type)
	if err == sql.ErrNoRows {
		return false, nil
	} else if err != nil {
		return false, err
	}
	return true, nil
}

// GetActivitysByUserID returns all Activities for a specific user
// takes user id as an input
func GetActivitysByUserID(db *sql.DB, uid string) ([]Activity, bool, error) {
	var acts []Activity
	// check if there are activities associeted with this user (uid)
	// and stop the method so it won't cause a memory leak
	ok, err := ActivityExist(db, uid)

	if !ok && err != nil {
		return nil, false, err
	} else if !ok {
		return nil, false, nil
	}

	rows, err := db.Query("select * from Activity where UserID = ?", uid)
	if err != nil {
		log.Println(err)
		return nil, false, err
	}
	defer rows.Close()
	for rows.Next() {
		act := Activity{}
		err = rows.Scan(&act.ID, &act.UserID, &act.Name, &act.Description, &act.Type)
		if err != nil {
			log.Println(err)
			return nil, false, err
		}
		acts = append(acts, act)
	}
	return acts, true, nil
}

func (ses *Session) InsertSession() bool {
	ses.ID = GenerateID()
	stmtIns, err := ses.DB.Prepare("INSERT Activity SET ID=?,ActivityID=?, Date=?,StartTime=?,EndTime=?")
	if err != nil {
		log.Println(err)
		return false
	}
	defer stmtIns.Close()
	_, err = stmtIns.Exec(ses.ID, ses.ActivityID, ses.Date, ses.StartTime, ses.EndTime)
	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}

// selects UserID of the owner of an activity
// returns (true,nil) if user owns this activity
// returns (false, nil ) if activity weren't found or this user doesn't own this activity
func (act *Activity)ActivityOwner() (bool, error){
	var uid string
	err := act.DB.QueryRow("SELECT UserID FROM Activity WHERE ID=?", act.ID).Scan(uid)

	if err == sql.ErrNoRows {
		return false, nil
	} else if err != nil {
		return false, err
	}
	if act.ID != uid {
		return false, nil
	}
	return true, nil
}

func (act *Activity) GetSessionsByID() ([]Session, bool, error) {
	var sess []Session

	// check if user owns this activity
	ok, err := act.ActivityOwner()
	if !ok {
		return nil, ok, err
	}


	rows, err := act.DB.Query("select * from Session where ActivityID = ?", act.ID)
	if err != nil {
		return sess, false, err
	}
	defer rows.Close()
	for rows.Next() {
		ses := Session{}
		err = rows.Scan(&ses.ID, &ses.ActivityID, &ses.Date, &ses.StartTime, &ses.EndTime)
		if err != nil {
			// better handle this one
			// FIXME
			panic(err)
		}
		sess = append(sess, ses)
	}
	return sess, true, nil
}
