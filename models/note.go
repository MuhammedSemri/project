package models

import (
	"database/sql"
)

type Note struct {
	DB        *sql.DB
	ID        string `json:"id"`
	SessionID string `json:"sessionid"`
	Content   string `json:"content"`
}
