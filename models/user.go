package models

import (
	"database/sql"
	"golang.org/x/crypto/bcrypt"
	"log"
)

// User the user data
type User struct {
	DB       *sql.DB
	ID       string
	Username string
	Email    string
	Password string
}

// UsernameExists checks if username already exists in the DB
// returns true if username already exists in te db
// func (usr *User) UsernameExists() bool {
// 	var usrname string

// 	err := usr.DB.QueryRow("SELECT Username FROM Users WHERE Username=?", usr.Username).Scan(&usrname)
// 	if err == sql.ErrNoRows {
// 		return false
// 	} else if err != nil {
// 		log.Fatal(err)
// 		return true
// 	}
// 	return true
// }

// EmailExists checks if a Email already exists in the db
// return true if it exists
// returns false if not
func (usr *User) EmailExists() bool {
	var email string

	err := usr.DB.QueryRow("SELECT Email FROM Users WHERE Email=?", usr.Email).Scan(&email)
	if err == sql.ErrNoRows {
		return false
	} else if err != nil {
		log.Fatal(err)
		return true
	}
	return true
}

// IDExists checks if UUID already exists in the DB
// returns true if it exists
func (usr *User) IDExists() bool {
	var id string

	err := usr.DB.QueryRow("SELECT UserID FROM Users WHERE UserID=?", usr.ID).Scan(&id)
	if err == sql.ErrNoRows {
		return false
	} else if err != nil {
		log.Fatal(err)
		return true
	}
	return true
}

func (usr *User) GetID() (bool, error) {
	var id string

	err := usr.DB.QueryRow("SELECT UserID FROM Users WHERE Email=?", usr.Email).Scan(&id)
	if err == sql.ErrNoRows {
		return false, nil
	} else if err != nil {
		log.Fatal(err)
		return false, err
	}
	usr.ID = id
	return true, nil
}

// CreateUser Inserts user data in the DB
// returns true if it was created successfuly
// returns false if there was an error
func (usr *User) CreateUser() bool {

	err := usr.HashPass()
	if err != nil {
		log.Fatal(err)
		return false
	}
	stmtIns, err := usr.DB.Prepare("INSERT Users SET UserID=?, User_type=?,Email=?,Pass=?")
	if err != nil {
		log.Println(err)
		return false
	}
	defer stmtIns.Close()
	_, err = stmtIns.Exec(usr.ID, "normal",  usr.Email, usr.Password)
	if err != nil {
		log.Fatal(err)
	}
	return true
}

// HashPass hashes the password using bcrypt
// returns error if encryption failed
func (usr *User) HashPass() error {

	hash, err := bcrypt.GenerateFromPassword([]byte(usr.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	usr.Password = string(hash)
	return nil
}

// GetUser Gets the user data from the db for login
// returns false if there is a problem
// if Email Doesn't exist in the db it returns false with nil error
func (usr *User) GetUser() (bool, error) {
	err := usr.DB.QueryRow("SELECT UserID,Pass FROM Users WHERE Email=?", usr.Email).Scan(&usr.ID, &usr.Password)
	if err == sql.ErrNoRows {
		return false, nil
	} else if err != nil {
		return false, err
	}
	return true, nil
}
