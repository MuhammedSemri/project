package models

import (
	"github.com/satori/go.uuid"
)

func GenerateID() string {
	uid := uuid.NewV4()
	return uid.String()
}
