package models

import (
	"database/sql"
	"log"
	"testing"

	_ "github.com/go-sql-driver/mysql"
)

var newAct = NewActivity(connectDB(), "test", "test", "test", "test")

func connectDB() *sql.DB {
	log.Println("running")
	db, err := sql.Open("mysql", "root:ls123456@/db")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Connected to the DB")
	return db
}
func TestActivity_InsertActivity(t *testing.T) {

	tests := []struct {
		name string
		act  *Activity
		want bool
	}{
		{
			name: "Save to db",
			act:  &newAct,
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.act.InsertActivity(); got != tt.want {
				t.Errorf("Activity.InsertActivity() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetActivitysByUserID(t *testing.T) {
	type args struct {
		db  *sql.DB
		uid string
	}
	tests := []struct {
		name    string
		args    args
		want1   bool
		wantErr bool
	}{
		{
			name: "get Activities",
			args: args{
				db:  connectDB(),
				uid: newAct.UserID,
			},
			want1:   true,
			wantErr: false,
		},
		{
			name: "get non existing User Activities",
			args: args{
				db:  connectDB(),
				uid: "a",
			},
			want1:   false,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, got1, err := GetActivitysByUserID(tt.args.db, tt.args.uid)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetActivityByUserID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got1 != tt.want1 {
				t.Errorf("GetActivityByUserID() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
