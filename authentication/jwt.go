package authentication

import (
	"crypto/rsa"
	jwt "github.com/dgrijalva/jwt-go"
	"io/ioutil"
	"log"
	"project/settings"
	"time"
)

// TODO: add blacklisting for token (for logout)

// JWTAuthenticationBackend contains the public and private rsa keys used to sign and validate tokens
type JWTAuthenticationBackend struct {
	privateKey *rsa.PrivateKey
	PublicKey  *rsa.PublicKey
}

const (
	tokenDuration = 72
	expireOffset  = 3600
)

var authBackendInstance *JWTAuthenticationBackend

// InitJWTAuthenticationBackend returns JWTAuthenticationBackend containing the RSA keys
func InitJWTAuthenticationBackend() *JWTAuthenticationBackend {
	if authBackendInstance == nil {
		authBackendInstance = &JWTAuthenticationBackend{
			privateKey: getPrivateKey(),
			PublicKey:  getPublicKey(),
		}
	}

	return authBackendInstance
}

// GenerateToken returns a new JWT token
// Take User ID as a parameter which is of type UUID(saved as string)
func (backend *JWTAuthenticationBackend) GenerateToken(userUUID string) (string, error) {
	token := jwt.New(jwt.SigningMethodRS512)
	token.Claims = jwt.MapClaims{
		"exp": time.Now().Add(time.Hour * time.Duration(settings.Get().JWTExpirationDelta)).Unix(),
		"iat": time.Now().Unix(),
		"sub": userUUID,
	}
	tokenString, err := token.SignedString(backend.privateKey)
	if err != nil {
		log.Printf("Error signing token. line %v", "50")
		log.Printf("Error: %v", err)
		return "", err
	}
	return tokenString, nil
}

// checks if the token is still valid and haven't expired yet
// returns tokens remaining validity
func (backend *JWTAuthenticationBackend) getTokenRemainingValidity(timestamp interface{}) int {
	if validity, ok := timestamp.(float64); ok {
		tm := time.Unix(int64(validity), 0)
		remainer := tm.Sub(time.Now())
		if remainer > 0 {
			return int(remainer.Seconds() + expireOffset)
		}
	}
	return expireOffset
}

// Gets RSA private key path from the json files
// and parses it into *rsa.PrivateKey then returns it
func getPrivateKey() *rsa.PrivateKey {
	// Get private key path
	privateKeyBytes, err := ioutil.ReadFile(settings.Get().PrivateKeyPath)
	if err != nil {
		log.Printf("Error reading private key: %s", settings.Get().PrivateKeyPath)
		log.Fatal(err)
	}
	// parse the RSA key into *rsa.PrivateKey
	privateKeyImported, err := jwt.ParseRSAPrivateKeyFromPEM(privateKeyBytes)
	return privateKeyImported
}

// Gets RSA key path from json file
// and parses it inot *rsa.PublicKey then returns it
func getPublicKey() *rsa.PublicKey {
	publicKeyBytes, err := ioutil.ReadFile(settings.Get().PublicKeyPath)
	if err != nil {
		log.Printf("Error reading private key: %s", settings.Get().PublicKeyPath)
		log.Fatal(err)
	}

	publicKeyImported, err := jwt.ParseRSAPublicKeyFromPEM(publicKeyBytes)
	return publicKeyImported
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
