package authentication

import (
	"fmt"
	jwt "github.com/dgrijalva/jwt-go"
	request "github.com/dgrijalva/jwt-go/request"
	"github.com/gorilla/context"
	"net/http"
)

// RequireTokenAuthentication is middlware to wrap http handlers
// it checks if a request contains a valid jwt token
// if not it returns a response to the user
func RequireTokenAuthentication(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	authBackend := InitJWTAuthenticationBackend()
	// parses the token from the request
	token, err := request.ParseFromRequest(req, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		// checks if the token is valid
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			// if not valid
			// TODO better handle the error
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return authBackend.PublicKey, nil

	})
	// if token is valid
	// TODO add if token is in blacklist
	if err == nil && token.Valid {
		// parse the user data from the token
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			context.Set(req, "claims", claims)
			fmt.Println(claims)
			// goto the wrapped func
			next(res, req)
		} else {
			res.WriteHeader(http.StatusUnauthorized)
		}

	} else {
		// if token is not valid respond with unauthorized
		res.WriteHeader(http.StatusUnauthorized)
	}
}

func ParseClaimsContext(req *http.Request) (mapped jwt.MapClaims) {

	cl := context.Get(req, "claims")
	mapped = cl.(jwt.MapClaims)
	return
}
