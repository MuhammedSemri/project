CREATE TABLE Users (
  UserID varchar(150) NOT NULL,
  User_type varchar(50)  NOT NULL,
  Email varchar(250) NOT NULL,
  Pass varchar(150) NOT NULL,
  PRIMARY KEY (`UserID`)
)ENGINE INNODB;

CREATE TABLE Activity(
  ID varchar(150) NOT NULL,
  UserID varchar(150) NOT NULL,
  ActivityName varchar(150) NOT NULL,
  Description varchar(250),
  ActivityType varchar(50)  NOT NULL,
  PRIMARY KEY (`ID`)
)ENGINE INNODB;

CREATE TABLE Session(
  ID varchar(150) NOT NULL,
  ActivityID varchar(150) NOT NULL,
  Location varchar(150),
  Date varchar(150) NOT NULL,
  StartTime varchar(50),
  EndTime varchar(50),
  PRIMARY KEY (`ID`)
)ENGINE INNODB;

CREATE TABLE Note(
  ID varchar(150) NOT NULL,
  ActivityID varchar(150) NOT NULL,
  Content varchar(250) NOT NULL,
  PRIMARY KEY (`ID`)
)ENGINE INNODB;
