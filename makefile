
run:
	go run cmd/main.go

build:
	go build cmd/main.go

runner:
	go build -o cmd/cmd.exe cmd/main.go
	cmd/cmd.exe

fmt:
	go fmt project/handlers
	go fmt project/models
	go fmt project/cmd/

dock:
	docker build -t AuthService .
	docker run --publish 6060:8080 --name AuthService --rm AuthService

clean:
	rm -f cmd\cmd.exe
	rm -f cmd\debug